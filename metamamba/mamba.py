import json
import logging
from os import path
from pathlib import Path
import subprocess
from time import monotonic
from typing import Any, Dict, List, NamedTuple, Union


_logger = logging.getLogger(__name__)


FileParam = Union[Path, str]
MetaData = Dict[str, Dict[str, Any]]


class ExecuteResult(NamedTuple):
    result: bytes
    seconds: float


class ExecuteJSONResult(NamedTuple):
    result: Any
    seconds: float


class ReadMetaMultiResult(NamedTuple):
    result: Dict[FileParam, MetaData]
    seconds: float


class ReadMetaResult(NamedTuple):
    result: MetaData
    seconds: float


class ExifToolVersion:
    def __init__(self, version: str):
        try:
            self.major, self.minor = map(int, version.split(".", 2))
        except:
            raise ValueError("could not parse ExifTool version: %s" % version)

    def __str__(self):
        return "%d.%02d" % (self.major, self.minor)

    def __lt__(self, other):
        return self.major < other.major \
            or (self.major == other.major and self.minor < other.minor)

    def __eq__(self, other):
        return self.major == other.major and self.minor == other.minor


class Mamba:
    _POPEN_ARGS = [
        "-stay_open", "true", "-@", "-",
    ]
    _READSIZE = 4096

    def __init__(self, exiftool: str = "exiftool"):
        more_args = []
        stderr = None

        self.exiftool_version = self.installed_exiftool_version(exiftool)
        _logger.info("Using ExifTool at '%s', version %s.",
            exiftool, self.exiftool_version)

        if self.exiftool_version < ExifToolVersion("12.10"):
            _logger.warn("You are running ExifTool %s. Versions < 12.10 do "
                "not fully support quiet operation when running in -stay_open "
                "mode. ExifTool's stderr output will be suppressed to "
                "compensate. Consider upgrading." % self.exiftool_version)
            stderr = subprocess.DEVNULL
        else:
            more_args.append("-q")

        cmd = [exiftool] + self._POPEN_ARGS + more_args
        _logger.debug("Connecting to ExifTool using %r.", cmd)
        self._popen = subprocess.Popen(
            cmd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=stderr,
        )
        self._cmdid = 0

    def __del__(self):
        if getattr(self, "_popen", None) is None:
            # We didn't initialize fully.
            return

        if self._popen.returncode is None:
            _logger.info("Closing ExifTool.")
            self.execute(b"-stay_open", b"0")
            self._popen.communicate()
            _logger.debug("ExifTool exited with %d." % self._popen.returncode)

    def _write(self, data: bytes):
        _logger.debug("--> %r", data)
        self._popen.stdin.write(data)
        self._popen.stdin.flush()

    def _read(self, cmdid: int) -> bytes:
        ack = b"{ready" + str(cmdid).encode() + b"}\n"
        acklen = len(ack)
        output = b""
        while output[-acklen:] != ack and not self._popen.stdout.closed:
            chunk = self._popen.stdout.read1(self._READSIZE)
            _logger.debug("<-- %r", chunk)
            if chunk == b"":
                break
            output += chunk
        return output[:-acklen]

    @staticmethod
    def installed_exiftool_version(exiftool: str = "exiftool",
                                  ) -> ExifToolVersion:
        res = subprocess.run([exiftool, "-ver"], stdout=subprocess.PIPE)
        res.check_returncode()
        return ExifToolVersion(res.stdout.decode())

    @staticmethod
    def filearg(filepath: FileParam, check_exists: bool = False) -> str:
        filepath = Path(filepath)
        if check_exists and not filepath.exists():
            raise FileNotFoundError("file '%s' does not exist" % filepath)
        if filepath.is_absolute():
            return str(filepath)
        return path.join(".", filepath)

    def execute(self, *args) -> ExecuteResult:
        self._cmdid += 1
        start = monotonic()
        self._write(b"\n".join([
            arg.encode() if isinstance(arg, str) else arg
            for arg in args
        ]) + b"\n-execute" + str(self._cmdid).encode() + b"\n")
        output = self._read(self._cmdid)
        return ExecuteResult(output, monotonic() - start)

    def execute_json(self, *args) -> ExecuteJSONResult:
        start = monotonic()
        return ExecuteJSONResult(
            json.loads(self.execute(b"-json", *args).result),
            monotonic() - start,
        )

    def read_meta(self, filepath: FileParam, human_readable: bool = False
                 ) -> ReadMetaResult:
        res = self.read_meta_multi([filepath], human_readable)
        return ReadMetaResult(
            next(iter(res.result.values())),
            res.seconds,
        )

    def read_meta_multi(self, filepaths: List[FileParam],
                        human_readable: bool = False) -> ReadMetaMultiResult:
        args = [b"-binary"]
        if not human_readable:
            args += [b"--printConv", b"-veryShort"]
        start = monotonic()
        res = self.execute_json(b"-groupHeadings", *args, *[
            self.filearg(filepath, True).encode() for filepath in filepaths
        ])
        return ReadMetaMultiResult(
            dict(zip(filepaths, res.result)),
            monotonic() - start,
        )
