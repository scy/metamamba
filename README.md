# MetaMamba

_A fast and modern Python interface to ExifTool._

This is supposed to become an opinionated interface to ExifTool.
There is no documentation yet, this is a work in progress.

Check out [`__main__.py`](metamamba/__main__.py) for a usage example, or run `python3 -m metamamba --log debug *.jpg`.
