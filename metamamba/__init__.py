from contextlib import contextmanager

from .mamba import Mamba


@contextmanager
def mamba(*args, **kwargs):
    instance = Mamba(*args, **kwargs)
    try:
        yield instance
    finally:
        del instance
