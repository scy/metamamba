import argparse
import logging

from . import mamba

# Make the output more beautiful, if we can.
try:
    import rich
    have_rich = True
    print = rich.print
except:
    have_rich = False


parser = argparse.ArgumentParser(description="Demo the metamamba library.")
parser.add_argument("--log", default="warning",
    help="select the log level to use",
    choices=["debug", "info", "warning", "error", "critical"])
parser.add_argument("files", nargs="+")
args = parser.parse_args()


logging.basicConfig(level=args.log.upper())


with mamba() as m:
    res = m.read_meta_multi(args.files)
    print(res.result)
    count = len(res.result)
    suffix = "'s" if count == 1 else "s'"
    print("Retrieved", count, f"file{suffix} metadata in",
        round(res.seconds, 3), "seconds.")
